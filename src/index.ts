import * as Koa from "koa";

export type GetRole = (ctx: Koa.Context) => Promise<string> | string;
export type Roles = Array<string> | string;
export type Props = {
	getRole: GetRole;
	denyHandler: Koa.Middleware;
};

export default class Role {
	private getRole: GetRole;
	private denyHandler: Koa.Middleware;

	public constructor({ getRole, denyHandler }: Props) {
		this.getRole = getRole;
		this.denyHandler = denyHandler;
	}

	public is(roles: Roles, denyHandler?: Koa.Middleware) {
		return async (ctx: Koa.Context, next: () => Promise<any>) => {
			try {
				const role = await this.getRole(ctx);
				if (roles === role || (Array.isArray(roles) && roles.includes(role))) {
					return next();
				} else {
					throw true;
				}
			} catch (e) {
				return denyHandler
					? await denyHandler(ctx, next)
					: await this.denyHandler(ctx, next);
			}
		};
	}
}
