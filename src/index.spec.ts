import * as Koa from "koa";

import Role from "./index";

describe("test role", () => {
	describe("sample role", () => {
		const ROLES = {
			GUEST: "guest",
			USER: "user",
			ADMIN: "admin"
		};
		let ctx: Koa.Context;
		const init = () => ({
			getRole: jest.fn(() => ROLES.USER),
			denyHandler: jest.fn(),
			localDenyHandler: jest.fn(),
			next: jest.fn()
		});

		test("should call next if role has access", async () => {
			const { getRole, denyHandler, localDenyHandler, next } = init();
			const role = new Role({ getRole, denyHandler });
			await role.is(ROLES.USER)(ctx, next);

			expect(getRole.mock.calls.length).toBe(1);
			expect(next.mock.calls.length).toBe(1);
			expect(denyHandler.mock.calls.length).toBe(0);
			expect(localDenyHandler.mock.calls.length).toBe(0);
		});

		test("should call next if roles has access", async () => {
			const { getRole, denyHandler, localDenyHandler, next } = init();
			const role = new Role({ getRole, denyHandler });
			await role.is([ROLES.USER, ROLES.ADMIN])(ctx, next);

			expect(getRole.mock.calls.length).toBe(1);
			expect(next.mock.calls.length).toBe(1);
			expect(denyHandler.mock.calls.length).toBe(0);
			expect(localDenyHandler.mock.calls.length).toBe(0);
		});

		test("should call denyHandler if role has no access", async () => {
			const { getRole, denyHandler, localDenyHandler, next } = init();
			const role = new Role({ getRole, denyHandler });
			await role.is([ROLES.ADMIN])(ctx, next);

			expect(getRole.mock.calls.length).toBe(1);
			expect(next.mock.calls.length).toBe(0);
			expect(denyHandler.mock.calls.length).toBe(1);
			expect(localDenyHandler.mock.calls.length).toBe(0);
		});

		test("should call localDenyHandler if role has no access", async () => {
			const { getRole, denyHandler, localDenyHandler, next } = init();
			const role = new Role({ getRole, denyHandler });
			await role.is(ROLES.ADMIN, localDenyHandler)(ctx, next);

			expect(getRole.mock.calls.length).toBe(1);
			expect(next.mock.calls.length).toBe(0);
			expect(denyHandler.mock.calls.length).toBe(0);
			expect(localDenyHandler.mock.calls.length).toBe(1);
		});
	});

	describe("sample async role", () => {
		const ROLES = {
			GUEST: "guest",
			USER: "user",
			ADMIN: "admin"
		};
		let ctx: Koa.Context;
		const init = () => ({
			getRole: jest.fn(() => Promise.resolve(ROLES.USER)),
			denyHandler: jest.fn(),
			localDenyHandler: jest.fn(),
			next: jest.fn()
		});

		test("should call next if role has access", async () => {
			const { getRole, denyHandler, localDenyHandler, next } = init();
			const role = new Role({ getRole, denyHandler });
			await role.is(ROLES.USER)(ctx, next);

			expect(getRole.mock.calls.length).toBe(1);
			expect(next.mock.calls.length).toBe(1);
			expect(denyHandler.mock.calls.length).toBe(0);
			expect(localDenyHandler.mock.calls.length).toBe(0);
		});

		test("should call next if roles has access", async () => {
			const { getRole, denyHandler, localDenyHandler, next } = init();
			const role = new Role({ getRole, denyHandler });
			await role.is([ROLES.USER, ROLES.ADMIN])(ctx, next);

			expect(getRole.mock.calls.length).toBe(1);
			expect(next.mock.calls.length).toBe(1);
			expect(denyHandler.mock.calls.length).toBe(0);
			expect(localDenyHandler.mock.calls.length).toBe(0);
		});

		test("should call denyHandler if role has no access", async () => {
			const { getRole, denyHandler, localDenyHandler, next } = init();
			const role = new Role({ getRole, denyHandler });
			await role.is([ROLES.ADMIN])(ctx, next);

			expect(getRole.mock.calls.length).toBe(1);
			expect(next.mock.calls.length).toBe(0);
			expect(denyHandler.mock.calls.length).toBe(1);
			expect(localDenyHandler.mock.calls.length).toBe(0);
		});

		test("should call localDenyHandler if role has no access", async () => {
			const { getRole, denyHandler, localDenyHandler, next } = init();
			const role = new Role({ getRole, denyHandler });
			await role.is(ROLES.ADMIN, localDenyHandler)(ctx, next);

			expect(getRole.mock.calls.length).toBe(1);
			expect(next.mock.calls.length).toBe(0);
			expect(denyHandler.mock.calls.length).toBe(0);
			expect(localDenyHandler.mock.calls.length).toBe(1);
		});
	});
});
