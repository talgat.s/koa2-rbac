# koa2-rbac

[![NPM version](https://img.shields.io/npm/v/koa2-rbac.svg?style=flat)](https://npmjs.org/package/koa2-rbac) [![Node.js Version](https://img.shields.io/node/v/koa2-rbac.svg?style=flat)](http://nodejs.org/download/) [![pipeline status](https://gitlab.com/talgat.s/koa2-rbac/badges/master/pipeline.svg)](https://gitlab.com/talgat.s/koa2-rbac/commits/master)

Simple rbac for [koa](https://github.com/koajs/koa) better use with [koa-router](https://github.com/ZijianHe/koa-router)

## Installation

    $ npm install koa2-rbac

## API

#### new Role(options)

| Param                 | Type                               | Description          |
| --------------------- | ---------------------------------- | -------------------- |
| [options]             | <code>Object</code>                | Options              |
| [options.getRole]     | <code>(ctx, next) => string</code> | return current role  |
| [options.denyHandler] | <code>(ctx, next) => void</code>   | default deny handler |

**Example**
Basic usage with `koa-router`, use [named routes](https://github.com/ZijianHe/koa-router#named-routes)(not required) to enable easy error message:

```javascript
const Koa = require("koa");
const Router = require("koa-router");
const Role = require("koa2-rbac");

const app = new Koa();
const router = new Router();
const role = new Role({
	getRole(ctx, next) {
		return ctx._user.role;
	},
	denyHandler(ctx, next) {
		const { _matchedRouteName: matchedRouteName } = ctx;
		ctx.status = 403;
		ctx.body = {
			error: matchedRouteName
				? `Access Denied - You don't have permission to :: ${matchedRouteName}`
				: "Access Denied - You don't have permission"
		};
	}
});
```

#### roles.is(roles, denyHanlder) => Koa.Middleware | void

| Param         | Type                                 | Description                    |
| ------------- | ------------------------------------ | ------------------------------ |
| [role]        | <code>string &#124; string[] </code> | Allowed roles                  |
| [denyHandler] | <code>(ctx, next) => string</code>   | deny handler for current route |

**Example**
Basic usage with `koa-router`

```javascript
router.patch("Update user", "/users/:id", role.is("ADMIN"), (ctx, next) => {
	// Only ADMIN allowed
});

router.post(
	"Send comment",
	"/comments",
	role.is(["ADMIN", "USER"]),
	(ctx, next) => {
		// Only ADMIN and USER allowed
	}
);

router.get("Get post", "/posts/:id", (ctx, next) => {
	// Everyone allowed, better to leave without role.is
});

router.delete(
	"Delete post",
	"/posts/:id",
	role.is(["ADMIN", "USER"], (ctx, next) => {
		ctx.status = 403;
		ctx.body = {
			error: "You cannot delete post"
		};
	}),
	(ctx, next) => {
		// Only ADMIN and USER allowed, for others returns "You cannot delete post"
	}
);
```

## License

MIT
